# WAn Checker Monitor

## About
This is a basic react app that functions as a GUI monitor for getting data from the WAN Checker CLI.

It is in a very early stage and may not work super well. It is also not very customizable at this point.

If you have any questions please reach out to me.

## WAN Checker CLI
The command line/terminal interface WAN checker that runs in the background and feeds data to this interface can be found here: [WAN Checker](https://gitlab.com/ToxicAirEvent/wan-checker)