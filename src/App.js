import React, { useEffect, useState } from 'react';
import axios from "axios";

import PingTable from './data-tables/pingTable.js';

import './App.css';

function App(){
  const [pingData, setPingData] = useState([]);
  const [hasData, setHasData] = useState(false);

  const allSiteHistory = async () => {
    let sitePings;

    try{
      sitePings = await axios.get("http://localhost:3001/all-sites");
    }catch(err){
      console.error("Error getting ping data.");
      console.error(err);
      return false;
    }

    setPingData(sitePings.data);
    setHasData(true);
  }

  useEffect(() => {
    allSiteHistory();
  }, []);

  setInterval(allSiteHistory, 60000);

  return(
      <div className="App">
        <h1>Welcome To The WAN Checker!</h1>
        <PingTable siteData={pingData} hasData={hasData} />
      </div>
  );
}

export default App;