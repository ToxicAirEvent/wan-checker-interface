import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";

import './index.css';
import App from './App';
import DomainInfo from './domainInfo';
import AboutPage from './AboutPage';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <Router>
    <div className="mainNav">
      <h1>WAN Checker</h1>
      <p><Link to="/">All Sites</Link> | <Link to="/about">About</Link></p>
    </div>

    <Routes>
      <Route exact path="/" element={<App />} />
      <Route path="/domain/:domain_nm" element={<DomainInfo />} />
      <Route path="/about" element={<AboutPage />} />
    </Routes>
  </Router>,
  root
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
