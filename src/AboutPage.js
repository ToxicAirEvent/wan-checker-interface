import React from 'react';

import './App.css';

function AboutPage(){
  return(
      <div className="App">
        <h1>What is this?</h1>
        <p>About two years ago I was having some trouble with my ISP.</p>
        <p>They said my internet was working. I said I couldn't connect to anything outside of the local network.</p>
        <p>This issue was intermittent and hard to prove. So I made this logger to check every few minutes if my PC could reach the outside world.</p>

        <p>How to use and install are on my GitLab: <a href="https://gitlab.com/ToxicAirEvent/wan-checker">https://gitlab.com/ToxicAirEvent/wan-checker</a></p>
      </div>
  );
}

export default AboutPage;