import React from 'react';
import {Link} from "react-router-dom";

function PingTable(props){

	/*
		Really actually need to catch if the history data of sites is null and decide to render the table or not based on that.
		As that is the real problem when no pings have occured yet.
		This does account for things like connection erros though.
	*/
	if(!props.hasData){
		return(
			<p>Nothing to report yet. Will check again in 1 minute.</p>
		);
	}

	return (
	  <table>
	    <thead>
	      <tr>
	        <td>Domain</td>
	        <td>Reachable?</td>
	        <td>Resolves To</td>
	        <td>Last Pinged</td>
	      </tr>
	    </thead>

	    <tbody>
	      {Object.keys(props.siteData).map((key, index) => {
	        /*
	          Could probably add some kind of poke function
	          when retrieved or on the backend but this will do for now.
	        */
	        let last_data = props.siteData[key]['history'][0];

	        return(
	          <tr key={index}>
	            <td><Link to={"/domain/" + key}>{key}</Link></td>
	            <td>{props.siteData[key]['last'] ? 'Yes' : 'No'}</td>
	            <td>{last_data['resolved']}</td>
	            <td>{last_data['date_time']}</td>
	          </tr>
	        );
	      })}
	    </tbody>
	  </table>
	);
}

export default PingTable;
