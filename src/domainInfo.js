import React, { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

import PingTable from './data-tables/pingTable.js';

import './App.css';

function PingHistoryRows(props){

  let historyArray = props.siteHistory;

  /*
    Interim boiler plate to handle display if there is no data for the table yet or it has not been loaded yet.
    It'd be better to set a state that then evaluates if the table should even run based on the historical data bein fetched successfully.
    But don't have time to handle all that error catching right this second.
  */
  if(historyArray == null || historyArray.length == 0){
    return(
      <tbody>
        <tr>
          <td>N/A</td>
          <td>N/A</td>
          <td>N/A</td>
        </tr>
      </tbody>
    );
  }

  return(
    <tbody>
      {historyArray.map((history, index) => {
        return(
          <tr key={index}>
            <td>{history.alive ? "Yes" : "No"}</td>
            <td>{history.resolved}</td>
            <td>{history.date_time}</td>
          </tr>
        );
      })}
    </tbody>
  );

}

function DomainInfo() {
  const {domain_nm} = useParams();
  const [isOnline, setIsOnline] = useState(false);
  const [queryHistory, setQueryHistory] = useState([]);

  const getPingHistory = async () => {
    let request_url = "http://localhost:3001/site-history/" + domain_nm;

    let single_domain = await axios.get(request_url);

    setIsOnline(single_domain.data.last);
    setQueryHistory(single_domain.data);
  };

  useEffect(() => {
    getPingHistory();
  }, []);

  setInterval(getPingHistory, 60000);

  return (
    <div className="App">
      <h1>Uptime Data For: {domain_nm}</h1>
      <h2>Currently Online: {isOnline ? "Yes" : "No"}</h2>

      <table>
        <thead>
          <tr>
            <td>Was Online?</td>
            <td>Resolves To</td>
            <td>Time Pinged</td>
          </tr>
        </thead>

        <PingHistoryRows siteHistory={queryHistory.history} />
      </table>
    </div>
  );
}

export default DomainInfo;
